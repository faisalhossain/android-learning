package com.shadowhite.android_learning.ui.home;

import android.util.Log;
import android.view.Gravity;
import android.view.View;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.shadowhite.android_learning.R;
import com.shadowhite.android_learning.base.BaseActivity;
import com.shadowhite.android_learning.databinding.ActivityHomeBinding;

public class HomeActivity extends BaseActivity {

    ActivityHomeBinding mBinding;
    NavigationView mNavigationView;
    DrawerLayout mDrawerLayout;
    ActionBarDrawerToggle mDrawerToogle;
    Toolbar toolbar;

    @Override
    public int setLayoutId() {
        return R.layout.activity_home;
    }

    @Override
    public void startUI() {
        mBinding = (ActivityHomeBinding) getViewDataBinding();
        setupNavigationView();

    }

    private void setupNavigationView() {
        mNavigationView = mBinding.navigationview;
        mDrawerLayout = mBinding.drawerLayout;

        toolbar = mBinding.mainappbar.toolbar;
        setSupportActionBar(toolbar);
        mDrawerToogle = new ActionBarDrawerToggle(
                this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(mDrawerToogle);
        mDrawerToogle.syncState();
        mNavigationView.setNavigationItemSelectedListener(item -> {
            showLongToast("" + item.getItemId());
            mDrawerLayout.closeDrawers();
            return false;
        });

//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                Log.d("chk","Clicked!!");
//                if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
//                    mDrawerLayout.closeDrawer(Gravity.LEFT);
//                } else {
//                    mDrawerLayout.openDrawer(Gravity.LEFT);
//                }
//            }
//        });
    }

}
