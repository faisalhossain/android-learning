package com.shadowhite.android_learning.util.remote.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RemoteApiProviderSaloon {


  // private static final String BASE_URL = AppConstraints.AppBaseUrl.APP_URL;
  // private static final String BASE_URL = "http://uysys10.pathshala24.com/mobileapp_api/api/";
   private static final String BASE_URL = "http://ujala.boihub.com/ujala/mobileapp_api/api/";
 //  private static final String BASE_URL = "http://uysshopapi.herokuapp.com/api/";
  // private static final String TOKEN = "Token 8eca7331cba1d1d611cf26c9c98090abe0a39064";
   private static final String TOKEN = "Token 154ea29d6ef92c998a7c8cab70a603151a049071";
    private static RemoteApiProviderSaloon mInstance;
    private Retrofit retrofit;
    Gson gson = new GsonBuilder()
            .setLenient()
            .create();
    OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request newRequest  = chain.request().newBuilder()
                   // .addHeader("Authorization", TOKEN)
                    .addHeader("Content-Type","application/x-www-form-urlencoded")
                    .build();
            return chain.proceed(newRequest);
        }
    }).build();

    private RemoteApiProviderSaloon() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
              //  .client(client)
              //  .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static synchronized RemoteApiProviderSaloon getInstance() {
        if (mInstance == null) {
            mInstance = new RemoteApiProviderSaloon();
        }
        return mInstance;
    }


    /**
     * get remote video api for featured and most popular
     * @return retrofit create
     */
    public RemoteApiInterfaceSaloon getRemoteApi(){
        return retrofit.create(RemoteApiInterfaceSaloon.class);
    }




    public static String getBaseUrl() {
        return BASE_URL;
    }
}
