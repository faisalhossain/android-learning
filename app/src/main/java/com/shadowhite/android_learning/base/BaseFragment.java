package com.shadowhite.android_learning.base;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.shadowhite.android_learning.util.AppConstraints;
import com.shadowhite.android_learning.util.SharedPrefUtil;


import java.text.SimpleDateFormat;
import java.util.Calendar;

public abstract class BaseFragment extends Fragment {
   public abstract int setLayoutId();
   private ViewDataBinding viewDataBinding;
   public abstract void startUI();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewDataBinding= DataBindingUtil.inflate(inflater,setLayoutId(),container,false);
        startUI();
        return viewDataBinding.getRoot();
    }




    public ViewDataBinding getViewDataBinding() {
        return viewDataBinding;
    }


    public String getCurrentTime()
    {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("DD-MM-yyyy HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }
    public void printCommonLog(String arg1,String arg2)
    {
        Log.d(AppConstraints.LogConstraints.COMMON_TAG,arg1+": "+arg2);
    }
}
